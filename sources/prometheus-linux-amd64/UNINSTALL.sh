#! /bins/bash

sudo rm /usr/local/bin/prometheus
sudo rm /usr/local/bin/promtool

userdel prometheus
sudo rm -r /var/lib/prometheus

sudo rm -r /usr/local/etc/prometheus
sudo rm /usr/local/etc/default/prometheus
sudo rm /etc/systemd/system/prometheus.service

sudo rm /usr/local/bin/prometheus-node-exporter

sudo rm /usr/local/etc/default/prometheus-node-exporter
sudo rm /etc/systemd/system/prometheus-node-exporter.service

sudo systemctl daemon-reload