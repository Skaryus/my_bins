#! /bin/bash

cwd="$(dirname "$0")"

sudo groupadd --system prometheus
sudo useradd --system \
    --gid prometheus \
    --create-home \
    --home-dir /var/lib/prometheus \
    --comment "Prometheus Metrics Exporter" \
    prometheus


sudo mv $cwd/prometheus-bin /usr/local/bin/prometheus
sudo mv $cwd/promtool-bin /usr/local/bin/promtool

sudo mkdir -p /usr/local/etc/default
sudo mkdir -p /usr/local/etc/prometheus

sudo mv $cwd/console_libraries  /usr/local/etc/prometheus/console_libraries
sudo mv $cwd/consoles /usr/local/etc/prometheus/consoles

sudo mv $cwd/prometheus /usr/local/etc/default/prometheus
sudo mv $cwd/prometheus.yaml /usr/local/etc/prometheus/prometheus.yaml
sudo mv $cwd/prometheus.service /etc/systemd/system/prometheus.service


sudo mv $cwd/node_exporter-bin /usr/local/bin/prometheus-node-exporter

sudo mv $cwd/prometheus-node-exporter /usr/local/etc/default/prometheus-node-exporter
sudo mv $cwd/prometheus-node-exporter.service /etc/systemd/system/prometheus-node-exporter.service

sudo chown prometheus:prometheus /usr/local/etc/default/prometheus
sudo chown prometheus:prometheus /usr/local/etc/default/prometheus-node-exporter
sudo chown -R prometheus:prometheus /usr/local/etc/prometheus

sudo systemctl daemon-reload