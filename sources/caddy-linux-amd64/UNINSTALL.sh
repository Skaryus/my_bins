#! /bin/bash

sudo rm /usr/local/bin/caddy

sudo userdel caddy
sudo rm -r /var/lib/caddy

sudo rm -r /usr/local/etc/caddy
sudo rm /usr/local/etc/default/caddy
sudo rm /usr/local/etc/default/caddy-api
sudo rm /etc/systemd/system/caddy.service
sudo rm /etc/systemd/system/caddy-api.service

sudo systemctl daemon-reload