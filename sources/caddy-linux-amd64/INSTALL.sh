#! /bin/bash

set -e


cwd="$(dirname "$0")"

sudo mv $cwd/caddy-bin /usr/local/bin/caddy
sudo groupadd --system caddy
sudo useradd --system \
    --gid caddy \
    --create-home \
    --home-dir /var/lib/caddy \
    --shell /usr/sbin/nologin \
    --comment "Caddy web server" \
    caddy

sudo mkdir -p /usr/local/etc/default
sudo mkdir -p /usr/local/etc/caddy

sudo mv $cwd/Caddyfile /usr/local/etc/caddy/Caddyfile
sudo mv $cwd/caddy-server.json /usr/local/etc/caddy/caddy-server.json
sudo mv $cwd/caddy /usr/local/etc/default/caddy
sudo mv $cwd/caddy-api /usr/local/etc/default/caddy-api
sudo mv $cwd/caddy.service /etc/systemd/system/caddy.service
sudo mv $cwd/caddy-api.service /etc/systemd/system/caddy-api.service

sudo chown caddy:caddy /usr/local/etc/caddy
sudo chown caddy:caddy /usr/local/etc/default/caddy
sudo chown -R caddy:caddy /usr/local/etc/default/caddy-api

sudo systemctl daemon-reload