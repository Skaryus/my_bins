#! /bin/bash

sudo rm /usr/local/bin/grafana-loki

sudo rm /usr/local/etc/default/grafana-loki
sudo rm -r /usr/local/etc/grafana-loki
sudo rm /etc/systemd/system/grafana-loki.service

sudo rm /usr/local/bin/grafana-loki-promtail

sudo rm /usr/local/etc/default/grafana-loki-promtail
sudo rm /etc/systemd/system/grafana-loki-promtail.service