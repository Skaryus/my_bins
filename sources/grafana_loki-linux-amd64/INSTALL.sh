#! /bin/bash

set -e

cwd="$(dirname "$0")"

sudo groupadd --system grafana-loki
sudo useradd --system \
    --gid grafana-loki \
    --comment "Grafana-Loki Log Exporter" \
    grafana-loki

sudo mv $cwd/loki-bin /usr/local/bin/grafana-loki

sudo mkdir -p /usr/local/etc/default
sudo mkdir -p /usr/local/etc/grafana-loki

sudo mv $cwd/grafana-loki /usr/local/etc/default/grafana-loki
sudo mv $cwd/grafana-loki.yaml /usr/local/etc/grafana-loki/grafana-loki.yaml
sudo mv $cwd/grafana-loki.service /etc/systemd/system/grafana-loki.service

sudo mv $cwd/promtail-bin /usr/local/bin/grafana-loki-promtail

sudo mv $cwd/grafana-loki-promtail /usr/local/etc/default/grafana-loki-promtail
sudo mv $cwd/grafana-loki-promtail.yaml /usr/local/etc/grafana-loki/grafana-loki-promtail.yaml
sudo mv $cwd/grafana-loki-promtail.service /etc/systemd/system/grafana-loki-promtail.service

sudo chown grafana-loki:grafana-loki /usr/local/etc/default/grafana-loki
sudo chown grafana-loki:grafana-loki /usr/local/etc/default/grafana-loki-promtail
sudo chown -R grafana-loki:grafana-loki /usr/local/etc/grafana-loki

sudo usermod -a -G adm grafana-loki